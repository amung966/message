package com.message.model;

public class Message {
	
	private String [] tid , title , msg , date , uname , aid , fid , ftid , file , ofile;
	
	public Message() {
		super();
	}
	
	public Message(String [] tid , String [] title , String [] msg , String [] date , String [] uname , 
			String [] file , String [] fid , String [] ftid , String [] ofile , String [] aid) {
		super();
		this.tid = tid;
		this.title = title;
		this.msg = msg;
		this.date = date;
		this.uname = uname;
		this.aid = aid;
		this.fid = fid;
		this.ftid = ftid;
		this.file = file;
		this.ofile = ofile;
	}
	
	public void setTid(String [] tid) {
		this.tid = tid;
	}
	
	public void setTitle(String [] title) {
		this.title = title;
	}
	
	public void setMsg(String [] msg) {
		this.msg = msg;
	}
	
	public void setDate(String [] date) {
		this.date = date;
	}
	
	public void setUname(String [] uname) {
		this.uname = uname;
	}
	
	public void setFile(String [] file) {
		this.file = file;
	}
	
	public void setoFile(String [] ofile) {
		this.ofile = ofile;
	}
	
	public void setFid(String [] fid) {
		this.fid = fid;
	}
	
	public void setFtid(String [] ftid) {
		this.ftid = ftid;
	}
	
	public void setAid(String [] aid) {
		this.aid = aid;
	}
	
	public String[] getTid() {
		return tid;
	}
	
	public String[] getTitle() {
		return title;
	}
	
	public String[] getMsg() {
		return msg;
	}
	
	public String[] getDate() {
		return date;
	}
	
	public String[] getUname() {
		return uname;
	}
	
	public String[] getFile() {
		return file;
	}
	
	public String[] getoFile() {
		return ofile;
	}
	
	public String[] getFid() {
		return fid;
	}
	
	public String[] getFtid() {
		return ftid;
	}
	
	public String[] getAid() {
		return aid;
	}
}

