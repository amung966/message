package com.message.servlet;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.message.server.DBConnection;
import com.message.server.MyClass;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		DBConnection db = new DBConnection();
		MyClass my = new MyClass();
		Long tss = System.currentTimeMillis()/1000;
		String ts = Long.toString(tss);
		String sql = "";
		ResultSet rs = null;
		
		//POST Value
		String account = request.getParameter("account");
		String uname = request.getParameter("uname");
		String passwd = request.getParameter("passwd");
		String rpasswd = request.getParameter("rpasswd");
		
		if("".equals(account) || "".equals(uname) || "".equals(passwd) || "".equals(rpasswd)) {
			session.setAttribute("failmsg", "please enter name or account or password");
			response.sendRedirect("register.jsp");
			return;
		}
		
		passwd = my.MD5(passwd);
		rpasswd = my.MD5(rpasswd);
		
		if(!passwd.equals(rpasswd)) {
			session.setAttribute("failmsg", "password and retype password not match");
			response.sendRedirect("register.jsp");
			return;
		}
		
		sql = "SELECT * FROM account WHERE account='"+account+"' OR name='"+uname+"'";
		rs = db.doSelectSql(sql);
		
		try {
			rs.last();
			if(rs.getRow() > 0) {
				session.setAttribute("failmsg", "account or name repeat");
				response.sendRedirect("register.jsp");
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			session.setAttribute("failmsg", "register error");
			response.sendRedirect("register.jsp");
			return;
		}
		
		
		sql = "INSERT INTO `account`(`account`, `passwd`, `name`, `a_created`, `a_changed`, `a_lasttime`) "
				+ "VALUES ('"+account+"','"+passwd+"','"+uname+"','"+ts+"','"+ts+"','"+ts+"')";
		
		int a = db.doInsertSql(sql);
		
		if(a > 0) {
			session.setAttribute("message", "registered success , you can login");
			response.sendRedirect("index.jsp");
			return;
		}else {
			session.setAttribute("failmsg", "registered error");
			response.sendRedirect("register.jsp");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
