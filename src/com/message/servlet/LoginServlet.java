package com.message.servlet;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.message.model.Users;
import com.message.server.DBConnection;
import com.message.server.MyClass;
import com.message.server.UserService;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		MyClass my = new MyClass();
		UserService dbuser = new UserService();
		Users user = new Users();
		
		//transform 方法一
//		RequestDispatcher success_rd = request.getRequestDispatcher("home.jsp");
//	    RequestDispatcher error_rd = request.getRequestDispatcher("index.jsp");
		
		//POST Value
		String account = request.getParameter("account");
		String passwd = request.getParameter("passwd");
		
		passwd = my.MD5(passwd);

		if(passwd == null || account == null) {
			session.setAttribute("failmsg", "please enter account or password");
	  		session.setAttribute("logined", "logout");
//	  		error_rd.forward(request, response);
	  		//轉頁方法二 會更改url
	  		response.sendRedirect("/message/LoginShow");
			return;
		}
		
		user = dbuser.hasuser(account, passwd);
		
		if(user.getName() == null) {
			System.out.println("false");
  			session.setAttribute("failmsg", "account or password error");
    	  		session.setAttribute("logined", "logout");
    	  		response.sendRedirect("/message/LoginShow");
    	  		return;
		}else {
			System.out.println("success");
	  		session.setAttribute("logined", "login");
	  		session.setAttribute("message", "Login success");
	  		session.setAttribute("user", user.getName());
	  		session.setAttribute("uid", user.getUid());
	  		response.sendRedirect("/message/MessageShow");
	  		return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
