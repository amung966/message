package com.message.servlet;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.message.server.DBConnection;
import com.message.server.MyClass;

/**
 * Servlet implementation class Setting
 */
@WebServlet("/Setting")
public class Setting extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Setting() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		DBConnection db = new DBConnection();
		MyClass my = new MyClass();
		Long tss = System.currentTimeMillis()/1000;
		String ts = Long.toString(tss);
		String sql = "";
		ResultSet rs = null;
		
		//POST Value
		String user = request.getParameter("user");
		String oldpasswd = request.getParameter("oldpasswd");
		String newpasswd = request.getParameter("newpasswd");
		String rnewpasswd = request.getParameter("rnewpasswd");
		
		if("".equals(user) || "".equals(oldpasswd) || "".equals(newpasswd) || "".equals(rnewpasswd)) {
			session.setAttribute("failmsg", "please enter password or new password or new retype password");
			response.sendRedirect("setting.jsp");
			return;
		}
		
		oldpasswd = my.MD5(oldpasswd);
		newpasswd = my.MD5(newpasswd);
		rnewpasswd = my.MD5(rnewpasswd);
		
		if(!newpasswd.equals(rnewpasswd)) {
			session.setAttribute("failmsg", "new password and new retype password not match");
			response.sendRedirect("setting.jsp");
			return;
		}
		
		sql = "SELECT * FROM account WHERE a_id='"+user+"' AND passwd='"+oldpasswd+"'";
		rs = db.doSelectSql(sql);
		
		try {
			rs.last();
			if(rs.getRow() == 0) {
				session.setAttribute("failmsg", "old password error");
				response.sendRedirect("setting.jsp");
				return;
			}
			
			sql = "UPDATE `account` SET `passwd`='"+newpasswd+"',`a_changed`='"+ts+"',"
					+ "`a_lasttime`='"+ts+"' WHERE a_id = '"+user+"'";
			
			int a = 0;
			a = db.doInsertSql(sql);
			if(a <= 0) {
				session.setAttribute("failmsg", "update password error");
				response.sendRedirect("setting.jsp");
				return;
			}
			
			session.setAttribute("message", "update success");
			response.sendRedirect("setting.jsp");
			return;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			session.setAttribute("failmsg", "update password error");
			response.sendRedirect("setting.jsp");
			return;
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
