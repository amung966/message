package com.message.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.message.model.Message;
import com.message.server.MessageService;
import com.message.server.MyClass;
import com.message.server.UploadService;

/**
 * Servlet implementation class MessageServlet
 */
@WebServlet("/MessageServlet")
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HttpSession session;
	private Long tss = System.currentTimeMillis()/1000;
	private String ts = Long.toString(tss);
	private UploadService up;
	private MyClass my = new MyClass();
	private Message msg = new Message();
	private MessageService dbmsg = new MessageService();
	private String user , info , title , type , filename , ofilename , tid;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		session = request.getSession();
		
		try {
			up = new UploadService(request);
			up.setUploadDir(getServletContext().getRealPath("/upload/"));
			//POST Value
			type = up.getParameter("type");
			if("1".equals(type)) {
				Add();
			}else if("2".equals(type)) {
				Upd();
			}else if("3".equals(request.getParameter("type"))) {
				Del(request);
			}else if("4".equals(request.getParameter("type"))) {
				DelFile(request);
			}else {
				session.setAttribute("error", type+" error");
			}
			
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			session.setAttribute("error", " error");
		}
		
		response.sendRedirect("/message/MessageShow");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private void Add() {
		System.out.println("add");
		info = up.getParameter("info");
		user = up.getParameter("user");
		title = up.getParameter("title");
		String ofile = up.getFileName();
		System.out.println("of="+ofile);
		if(user == null ||info == null || title == null) {
			session.setAttribute("error", "please enter msg");
			return;
		}
		
		if(up.isExtUpload("file")) {
			filename = UploadFile("");
		}else {
			filename = "";
		}
		System.out.println("fn="+filename);
		boolean status = dbmsg.insertMsg(user, title, info , filename , ofile , ts);
		if(status) {
			session.setAttribute("message", "insert success");
		}else {
			session.setAttribute("error", "insert error");
		}
	}
	
	private void Upd() {
		System.out.println("upd");
		info = up.getParameter("info");
		title = up.getParameter("title");
		tid = up.getParameter("who");
		ofilename = up.getFileName();
		
		if(info == null || title == null || tid == null) {
			session.setAttribute("error", "please enter msg");
			return;
		}
		
		if(up.isExtUpload("file")) {
			filename = UploadFile("");
		}else {
			filename = "";
		}
		
		boolean status = dbmsg.updMsg(tid, title, info , filename , ofilename , ts);
		
		if(status) {
			session.setAttribute("message", "upd success");
		}else {
			session.setAttribute("error", "upd error");
		}
		
	}
	
	private void Del(HttpServletRequest request) {
		System.out.println("del");
		tid = request.getParameter("who");
		ofilename = request.getParameter("file");
		
		if(!"".equals(ofilename)) {
			boolean file = my.deleteFile(getServletContext().getRealPath("/upload/")+ofilename);
		}
		
		boolean status = dbmsg.delMsg(tid);
		
		if(status) {
			session.setAttribute("message", "delete success");
		}else {
			session.setAttribute("error", "delete error");
		}
	}
	
	private String UploadFile(String oldfilename) {
		
		if(!"".equals(oldfilename)) {
			boolean file = my.deleteFile(getServletContext().getRealPath("/upload/")+oldfilename);
		}
		filename = (String)ts.subSequence(5,9)+my.randomString(5);
		up.doUpload("file", filename);
		filename = up.getFileName();

		return filename;
	}
	
	private String DelFile(HttpServletRequest request) {
		System.out.println("DelFile");
		String fid = request.getParameter("tid");
		ofilename = request.getParameter("info");
		String target = getServletContext().getRealPath("/upload/"+ofilename);
		System.out.println(target);
		my.deleteFile(target);
		
		boolean status = dbmsg.delFile(fid , "One");
		if(!status) {
			return "input";
		}
		return "success";
	}

}
