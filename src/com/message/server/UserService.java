package com.message.server;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.message.model.Users;
import com.mysql.jdbc.Connection;

public class UserService {
	
	private DBConnection db;
	
	public UserService() {
		db = new DBConnection();
	}
	
	public Users hasuser(String account , String passwd) {
		
		Users user = new Users();
		Connection con = db.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM account WHERE account = ? and passwd = ?";
		
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, passwd);
			rs = db.doSelectSqlPS(ps);
			
			if(rs.next()) {
				user.setUid(rs.getString("a_id"));
				user.setAccount(rs.getString("account"));
				user.setPasswd(rs.getString("passwd"));
				user.setName(rs.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			db.closeDB(con , rs , ps);
		}
		
		return user;
	}
	
}
