package com.message.server;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.message.model.Message;
import com.mysql.jdbc.Connection;

public class MessageService {
	
	private DBConnection db;
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;
	private MyClass my;
	
	
	public MessageService() {
		my = new MyClass();
		db = new DBConnection();
		con = db.getConnection();
		ps = null;
		rs = null;
	}
	
	public Message getMsg() {
		
		Message message = new Message();
		String []tid , msg , date , uname , file , ofile , aid , title , fid , ftid;
		
		String sql = "select * from message_view";
		
		try {
			rs = db.doSelectSql(sql);
			rs.last();
			int cou = rs.getRow();
			
			tid = new String[cou];
			msg = new String[cou];
			date = new String[cou];
			uname = new String[cou];
			aid = new String[cou];
			title = new String[cou];
			
			rs.first();
			
			for(int i=0;i<cou;i++) {
				tid[i] = rs.getString(1);
				msg[i] = rs.getString(3);
				date[i] = my.Date(Long.parseLong(rs.getString(5)));
				uname[i] = rs.getString(11);
				aid[i] = rs.getString(2);
				title[i] = rs.getString(4);
				rs.next();
			}
			
			sql = "SELECT * FROM file";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.last();
			int couu = rs.getRow();
			
			fid = new String[couu];
			ftid = new String[couu];
			file = new String[couu];
			ofile = new String[couu];
			rs.first();
			for(int i=0; i<couu; i++) {
				fid[i] = rs.getString(1);
				ftid[i] = rs.getString(2);
				file[i] = rs.getString(3);
				ofile[i] = rs.getString(4);
				rs.next();
			}
			
			message.setTid(tid);
			message.setMsg(msg);
			message.setDate(date);
			message.setUname(uname);
			message.setAid(aid);
			message.setTitle(title);
			message.setFid(fid);
			message.setFtid(ftid);
			message.setFile(file);
			message.setoFile(ofile);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			db.closeDB(con , rs , ps);
		}
		
		return message;
	}
	
	public boolean insertMsg(String user , String title , String info , String file , String ofile , String time) {
		
		String sql = "INSERT INTO message (t_a_id , t_info , t_title , t_created , t_changed , t_lasttime) VALUES (?,?,?,?,?,?)";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, user);
			ps.setString(2, info);
			ps.setString(3, title);
			ps.setString(4, time);
			ps.setString(5, time);
			ps.setString(6, time);
			ps.executeUpdate();
			
			sql = "SELECT t_id FROM message_view WHERE t_created = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, time);
			rs = ps.executeQuery();
			rs.last();
			String tid = rs.getString(1);
			
			if(!"".equals(file)) {
				boolean status = insertFile(tid, file , ofile , time);
				if(!status) {
					delMsg(tid);
					return false;
				}
				
			}
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	public boolean updMsg(String tid , String title , String info , String file , String ofile , String time) {
		
		String sql = "UPDATE message SET t_info = ? , t_title = ?, t_changed = ? , t_lasttime = ?" + " WHERE t_id = ?";
		
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, info);
			ps.setString(2, title);
			ps.setString(3, time);
			ps.setString(4, time);
			ps.setString(5, tid);
			ps.executeUpdate();
			
			if(!"".equals(file)) {
				boolean status = insertFile(tid, file , ofile , time);
				if(!status) {
					delMsg(tid);
					return false;
				}
				
			}
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean delMsg(String tid) {
		
		String sql = "DELETE FROM message where t_id = ?";
		
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, tid);
			ps.executeUpdate();
			
			delFile(tid , "All");
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}
	
	private boolean insertFile(String tid , String file , String ofile , String time) {
		
		String sql = "INSERT INTO file (f_t_id , f_name , f_o_name , f_created , f_changed , f_lasttime)"
				+ "VALUES (?,?,?,?,?,?)";
			
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, tid);
			ps.setString(2, file);
			ps.setString(3, ofile);
			ps.setString(4, time);
			ps.setString(5, time);
			ps.setString(6, time);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
			
		
			
		return true;
		
	}
	
	public boolean delFile(String fid , String type) {
		System.out.println(fid);
		String sql = null;
		
		if("All".equals(type)) {
			sql = "DELETE FROM file where f_t_id = ?";
		}else if("One".equals(type)) {
			sql = "DELETE FROM file where f_id = ?";
		}
		
		
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, fid);
			ps.executeUpdate();
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
