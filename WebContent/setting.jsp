<%@ page import ="java.util.*" %>
<%@ page import="java.sql.*"%> 
<%@ page import="java.text.*"%> 
<%@ page import="com.message.server.MyClass" %>
<%@ page import="com.message.server.DBConnection" %>
<%
	String logined = (String) session.getAttribute("logined");
	if(logined != null){
		if("logout".equals(logined)){
			response.setHeader("Refresh", "0; " + "index.jsp");
			return;
		}
	}else{
		response.setHeader("Refresh", "0; " + "index.jsp");
		return;
	}
	String uid = (String) session.getAttribute("uid");

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Yi's Message Board</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7-->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <style type="text/css">
    .mbox{
      z-index: 100;
      position: absolute;
      width: 30%;
    }
  </style>
  <script type="text/javascript">
    jQuery(document).ready(function(){
      //不管訊息視窗是否有啟動，頁面載入2秒後都關閉
      setTimeout(function(){
        $('.mbox').fadeOut();
      }, 2000);
    });
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><%out.write((String) session.getAttribute("user"));%></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
                <p>
                  <%out.write((String) session.getAttribute("user"));%>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
              	<div class="pull-left">
                  <a href="setting.jsp" class="btn btn-default btn-flat">Setting</a>
                </div>
                <div class="pull-right">
                  <a href="Logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
<!--           <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul> -->
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<%
		String error = (String) session.getAttribute("error");
		if(error != null){
			out.write("<div class='alert alert-warning alert-dismissible mbox'>");
			out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
			out.write("<h4><i class='icon fa fa-warning'></i> warning!</h4>");
			out.write(error);
			out.write("</div>");
			session.removeAttribute("error");
		}
		String message = (String) session.getAttribute("message");
		if(message != null){
			out.write("<div class='alert alert-success alert-dismissible mbox'>");
			out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
			out.write("<h4><i class='icon fa fa-check'></i> success!</h4>");
			out.write(message);
			out.write("</div>");
			session.removeAttribute("message");
		}
		String failmsg = (String) session.getAttribute("failmsg");
		if(failmsg != null){
			out.write("<div class='alert alert-danger alert-dismissible mbox'>");
			out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
			out.write("<h4><i class='icon fa fa-ban'></i> success!</h4>");
			out.write(failmsg);
			out.write("</div>");
			session.removeAttribute("failmsg");
		}
	%>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        		<div class="box-body">        		
		        <form action="Setting" method="POST">
		        		<h2>Update Password</h2><br>
	        			<input type="hidden" value="<% out.write((String) session.getAttribute("uid"));%>" name="user" required>
					<div class="form-group">
						<input type="password" name="oldpasswd" placeholder="old password" class="form-control" required>
					</div>
					<div class="form-group">
						<input type="password" name="newpasswd" placeholder="new password" class="form-control" required>
					</div>
					<div class="form-group">
						<input type="password" name="rnewpasswd" placeholder="retype new password" class="form-control" required>
		        		</div>
		        		<div class="form-group">
						<button class="btn btn-success">Send</button>
						<a href="home.jsp" class="btn btn-danger">Cancel</a>
		        		</div>
		        	</form>
	        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer hidden-print">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.1<br>
    </div>
    <strong>Copyright &copy; 2017 PowerBy Yi</strong> All rights reserved. <br>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
	$(document).ready(function(){

	});
</script>
</body>
</html>
