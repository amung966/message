<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function(){
      //不管訊息視窗是否有啟動，頁面載入2秒後都關閉
      setTimeout(function(){
        $('#mbox').fadeOut();
      }, 2000);
    });
  </script>
</head>
<body class="hold-transition register-page">
<div class="register-box">
 <%
	String error = (String) session.getAttribute("error");
	if(error != null){
		out.write("<div class='alert alert-warning alert-dismissible' id='mbox'>");
		out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
		out.write("<h4><i class='icon fa fa-warning'></i> warning!</h4>");
		out.write(error);
		out.write("</div>");
		session.removeAttribute("error");
	}
	String message = (String) session.getAttribute("message");
	if(message != null){
		out.write("<div class='alert alert-success alert-dismissible' id='mbox'>");
		out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
		out.write("<h4><i class='icon fa fa-check'></i> success!</h4>");
		out.write(message);
		out.write("</div>");
		session.removeAttribute("message");
	}
	String failmsg = (String) session.getAttribute("failmsg");
	if(failmsg != null){
		out.write("<div class='alert alert-danger alert-dismissible' id='mbox'>");
		out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
		out.write("<h4><i class='icon fa fa-ban'></i> success!</h4>");
		out.write(failmsg);
		out.write("</div>");
		session.removeAttribute("failmsg");
	}
%>
  <div class="register-logo">
    <a href="index.jsp"><b>Yi Message</b>Board</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="Register" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="uname" class="form-control" placeholder="Full name" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="account" class="form-control" placeholder="Account" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="passwd" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="rpasswd" class="form-control" placeholder="Retype password" required>
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="index.jsp" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script>
  
</script>
</body>
</html>
