<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String logined = (String) session.getAttribute("logined");
	
	if(logined != null){
		if("login".equals(logined)){
			response.setHeader("Refresh", "0; " + "/message/MessageShow");
		}
	}
	

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Yi Message Board</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script type="text/javascript">
    jQuery(document).ready(function(){
      //不管訊息視窗是否有啟動，頁面載入2秒後都關閉
      setTimeout(function(){
        $('#mbox').fadeOut();
      }, 2000);
    });
  </script>
</head>
<body class="hold-transition login-page">
<div class="login-box">
<%
	String error = (String) session.getAttribute("error");
	if(error != null){
		out.write("<div class='alert alert-warning alert-dismissible' id='mbox'>");
		out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
		out.write("<h4><i class='icon fa fa-warning'></i> warning!</h4>");
		out.write(error);
		out.write("</div>");
		session.removeAttribute("error");
	}
	String message = (String) session.getAttribute("message");
	if(message != null){
		out.write("<div class='alert alert-success alert-dismissible' id='mbox'>");
		out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
		out.write("<h4><i class='icon fa fa-check'></i> success!</h4>");
		out.write(message);
		out.write("</div>");
		session.removeAttribute("message");
	}
	String failmsg = (String) session.getAttribute("failmsg");
	if(failmsg != null){
		out.write("<div class='alert alert-danger alert-dismissible' id='mbox'>");
		out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
		out.write("<h4><i class='icon fa fa-ban'></i> success!</h4>");
		out.write(failmsg);
		out.write("</div>");
		session.removeAttribute("failmsg");
	}
%>
  <div class="login-logo">
    <b>Yi Message </b>Board
  </div>
  <div class="login-box-body">
    <form action="Login" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="account" class="form-control" placeholder="account" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="passwd" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="register.jsp" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
