-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 30, 2017 at 05:14 PM
-- Server version: 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `talk`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `a_id` int(11) NOT NULL,
  `account` varchar(50) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `name` varchar(20) NOT NULL,
  `a_created` int(11) NOT NULL,
  `a_changed` int(11) NOT NULL,
  `a_lasttime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`a_id`, `account`, `passwd`, `name`, `a_created`, `a_changed`, `a_lasttime`) VALUES
(1, 'root', '63a9f0ea7bb98050796b649e85481845', 'root', 1497811252, 1501434834, 1501434834),
(2, 'root123', 'aabb2100033f0352fe7458e412495148', 'root123', 1501433246, 1501433246, 1501433246);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `t_id` int(11) NOT NULL,
  `t_a_id` int(11) NOT NULL,
  `t_info` text NOT NULL,
  `t_file` varchar(100) NOT NULL,
  `t_created` int(11) NOT NULL,
  `t_changed` int(11) NOT NULL,
  `t_lasttime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`t_id`, `t_a_id`, `t_info`, `t_file`, `t_created`, `t_changed`, `t_lasttime`) VALUES
(10, 1, 'asd', '5896emarp.jpg', 1501358965, 1501358965, 1501358965),
(12, 1, 'sd', '59549eoah.xlsx', 1501359542, 1501359542, 1501359542),
(18, 2, 'HIHI', '3326xxs85.jpg', 1501433264, 1501433264, 1501433264);

-- --------------------------------------------------------

--
-- Stand-in structure for view `message_view`
-- (See below for the actual view)
--
CREATE TABLE `message_view` (
`t_id` int(11)
,`t_a_id` int(11)
,`t_info` text
,`t_file` varchar(100)
,`t_created` int(11)
,`t_changed` int(11)
,`t_lasttime` int(11)
,`a_id` int(11)
,`account` varchar(50)
,`passwd` varchar(255)
,`name` varchar(20)
,`a_created` int(11)
,`a_changed` int(11)
,`a_lasttime` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `users_session`
--

CREATE TABLE `users_session` (
  `session_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `a_id` int(11) NOT NULL,
  `host` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Structure for view `message_view`
--
DROP TABLE IF EXISTS `message_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `message_view`  AS  select `message`.`t_id` AS `t_id`,`message`.`t_a_id` AS `t_a_id`,`message`.`t_info` AS `t_info`,`message`.`t_file` AS `t_file`,`message`.`t_created` AS `t_created`,`message`.`t_changed` AS `t_changed`,`message`.`t_lasttime` AS `t_lasttime`,`account`.`a_id` AS `a_id`,`account`.`account` AS `account`,`account`.`passwd` AS `passwd`,`account`.`name` AS `name`,`account`.`a_created` AS `a_created`,`account`.`a_changed` AS `a_changed`,`account`.`a_lasttime` AS `a_lasttime` from (`message` join `account` on((`message`.`t_a_id` = `account`.`a_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`t_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
